import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/main/Home.vue'
import Training from "../views/main/Training";
import Profile from "../views/main/Profile";
import Login from "../views/useraccount/Login";
import Team from "../views/about/Team";
import Experience from "../views/main/Experience";
import Contact from "../views/about/Contact";


Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
        alias: '/home'
    },
    {
        path: '/register',
        name: 'register',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/useraccount/Register.vue')
    },

    {
        path: '/training',
        name: 'Training',
        component: Training

    },
    {
        path: '/profile',
        name: 'Profile',
        component: Profile
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/team',
        name: 'Team',
        component: Team
    }
    ,
    {
        path: '/experience',
        name: 'Experience',
        component: Experience
    }
    ,
    {
        path: '/contact',
        name: 'Contact',
        component: Contact
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
    scrollBehavior(to,from,savedPosition){

        if (savedPosition) {

        return savedPosition
        }
        else
        return {x:0,y:0}
        
    }
})

export default router

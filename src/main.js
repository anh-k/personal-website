import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import vuetify from './plugins/vuetify';
import store from "./store";
import VueI18n from 'vue-i18n'
import AOS from 'aos';
import "aos/dist/aos.css";

Vue.use(VueI18n)
Vue.use(vuetify)

let locale = "fr-FR";
const i18n = new VueI18n({
  fallbackLocale:"fr-FR",
  locale:locale
})



new Vue({
  created() {
    AOS.init();
  },
  i18n,
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
